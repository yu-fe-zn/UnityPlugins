# UnityPlugins

#### 项目介绍

UnityPlugins，Unity工程Anroid、iOS相关插件

#### 目前功能

1. Android截屏插入相册 2016.05.15
2. iOS截屏插入相册 2018.05.16
3. Android选择相册图片加载 2019.07.18
4. iOS选择相册图片加载 2019.07.22
5. Android微信授权登录分享文字图片链接 2019.08.05
6. iOS微信分享文字图片链接 2019.08.07
7. 拉起Android拍照显示到Unity里 2020.04.28
  
#### 使用说明

1. Android文件夹为 Android Studio工程，Terminal 输入命令 gradlew makeJar(Win)或 gradle makeJar(Mac) 打包jar，
输出在Android\app\build\libs下，拷贝UnityPlugin.jar到Unity工程Unity\Assets\Plugins\Android\bin下 
2. 使用微信相关请修改Android Studio工程的包名以及Unity工程中UnityPlugins.cs的AppID和AppSecret，
我的微信开放平台未认证，只能进行分享

![Image text](https://gitee.com/awnuxcvbn/UnityPlugins/raw/master/Doc/Images/1.jpg)

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request