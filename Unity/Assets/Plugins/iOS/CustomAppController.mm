#import "UnityAppController.h"
#import "WXApi.h"
#import "WXApiObject.h"
#import "WXApiManager.h"

@interface CustomAppController : UnityAppController
@end

IMPL_APP_CONTROLLER_SUBCLASS (CustomAppController)

@implementation CustomAppController

// 向Unity传递参数；
extern void UnitySendMessage(const char *, const char *, const char *);
// 启动信息
NSString *m_launchInfo = @"";

extern "C"
{
    void GetLaunchInfo(const char *objName, const char *funcName);
}

// 获取启动参数
void GetLaunchInfo(const char *objName, const char *funcName)
{
    BOOL result1 = [m_launchInfo isEqualToString:@""];
    BOOL result2 = [m_launchInfo hasPrefix:@"wx"];
    if (!result1&&!result2)
    {
        NSLog(@"GetLaunchInfo m_launchInfo:%@",m_launchInfo);
        UnitySendMessage( objName,funcName, [m_launchInfo UTF8String]);
        // 清空，防止造成干扰;
        m_launchInfo = @"";
    }
    else
    {
        NSLog(@"GetLaunchInfo m_launchInfo:%@",m_launchInfo);
    }
}

//@"";
extern "C"
{
    void WxInit(const char *appId, const char *appSecret);
}
//微信初始化
void WxInit(const char *appId, const char *appSecret)
{
    NSLog(@"WxInit appId:%s",appId);
    NSLog(@"WxInit appSecret:%s",appSecret);
    NSString *WxAppId = [NSString stringWithFormat:@"%s",appId];
    [WXApi registerApp:WxAppId];
}

extern "C"
{
    void WxLogin(const char *objName, const char *funcName);
}
//微信登录
void WxLogin(const char *objName, const char *funcName)
{
    NSLog(@"WxLogin objName:%s",objName);
    NSLog(@"WxLogin funcName:%s",funcName);
    if(![WXApi isWXAppInstalled])
    {
        //微信未安装
        return;
    }
    NSString*str0 = [NSString stringWithFormat:@"%s", objName];
    NSString*str1 = @";";
    str1 = [str0 stringByAppendingString: str1];
    NSString*str2 = [NSString stringWithFormat:@"%s", funcName];
    str2 = [str1 stringByAppendingString: str2]; ;
    
    SendAuthReq* req = [[SendAuthReq alloc] init];
    req.scope = @"snsapi_userinfo";
    req.state = [NSString stringWithFormat:@"%@", str2];
    [WXApi sendReq:req];
}

extern "C"
{
    void WxShareText(const char *content,const int scene);
}
//微信分享文字
void WxShareText(const char *content,const int scene)
{
    if(![WXApi isWXAppInstalled])
    {
        //微信未安装
        return;
    }
    NSString *str0 = [NSString stringWithUTF8String:content];
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc]init];
    req.text = str0;
    req.bText = YES;
    req.scene = scene;
    [WXApi sendReq:req];
}

extern "C"
{
    void WxShareImg(const char *imgName,const int scene);
}
//微信分享图片
void WxShareImg(const char *imgName, const int scene)
{
    if(![WXApi isWXAppInstalled])
    {
        //微信未安装
        return;
    }
    NSLog(@"WxShareImg imgName:%s",imgName);
    NSString *strReadAddr = [NSString stringWithUTF8String:imgName];
    UIImage *img = [UIImage imageWithContentsOfFile:strReadAddr];
    NSLog(@"%@", [NSString stringWithFormat:@"w:%f, h:%f", img.size.width, img.size.height]);
    
    WXMediaMessage *message = [WXMediaMessage message];
    [message setThumbImage:img];
    
    WXImageObject *imageObject = [WXImageObject object];
    imageObject.imageData = UIImagePNGRepresentation(img);
    message.mediaObject = imageObject;
    
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc]init];
    req.message = message;
    req.bText = false;
    req.scene = scene;
    [WXApi sendReq:req];
}
extern "C"
{
    void WxShareUrl(const char *title, const char *description,const char *imgName,const char *url,const int scene);
}
//微信分享链接
void WxShareUrl(const char *title, const char *description,const char *imgName,const char *url,const int scene)
{
    if(![WXApi isWXAppInstalled])
    {
        //微信未安装
        return;
    }
    NSString *strReadAddr = [NSString stringWithUTF8String:imgName];
    UIImage *img = [UIImage imageWithContentsOfFile:strReadAddr];
    NSLog(@"%@", [NSString stringWithFormat:@"w:%f, h:%f", img.size.width, img.size.height]);
    WXMediaMessage *message = [WXMediaMessage message];
    message.title = [NSString stringWithUTF8String:title];
    message.description = [NSString stringWithUTF8String:description]; ;
    [message setThumbImage:img];
    WXWebpageObject *webpageObject = [WXWebpageObject object];
    webpageObject.webpageUrl = [NSString stringWithUTF8String:url];
    message.mediaObject = webpageObject;
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc]init];
    req.bText = false;
    req.message = message;
    req.scene = scene;
    
    [WXApi sendReq:req];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
    m_launchInfo = [url absoluteString];
    NSLog(@"openURL1 m_launchInfo:%@",m_launchInfo);
    if(![m_launchInfo isEqualToString:@""])
    {
        return  [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    }
    else
    {
        return true;
    }
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    m_launchInfo = [url absoluteString];
    NSLog(@"openURL2 m_launchInfo:%@",m_launchInfo);
    if(![m_launchInfo isEqualToString:@""])
    {
        return  [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    }
    else
    {
        return true;
    }
}
@end
